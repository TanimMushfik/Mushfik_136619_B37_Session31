<?php

interface CanFly{

    public function fly();
}

interface CanSwim{

    public function swim();
}

class Bird{
    public function info(){
        echo "I am a {$this->name}\n";
        echo "I am a bird" ." ";
    }
}

class Balbasor extends Bird  implements CanSwim {
    public $name="Balbasor";
    public function swim()
    {
        echo "I can swim";

    }

}

class Dove extends Bird implements CanFly{
   public $name="Dove";
    public function fly()
    {
        echo "I can Fly";
    }
}

class Duck extends Bird implements CanFly,CanSwim{
    public $name="Duck";
    public function fly()
    {
        echo "I can fly";
    }

    public function swim()
    {
        echo "I can Swim";
    }
}

function describe($bird)
{
    if ($bird instanceof Bird) {
        $bird->info();

    }
    if ($bird instanceof CanFly) {
        $bird->fly();
    }
    if ($bird instanceof CanSwim) {
        $bird->swim();
    }
}

describe(new Balbasor());