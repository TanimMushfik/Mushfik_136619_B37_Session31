<?php


abstract class AbstractClass{

    public function printOut(){
        print $this->getValue(). "\n";
    }
}

class ABC extends AbstractClass{

    public function test(){
        echo "this is a test";
    }
}

